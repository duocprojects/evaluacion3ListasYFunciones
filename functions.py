import time

def verificarRut(rut, usuarios):
    """
    Función para comprobar si existe un rut dentro de una lista de usuarios.

    Args:
        rut (str): El rut a comprobar si existe.

    Returns:
        bool: True si el RUT existe en la lista de usuarios, caso contrario devuelve False.
    """
    for usuario in usuarios:
        if rut == usuario["rut"]:
            return True
    return False
    
# Validación de RUT
def inputRut(usuarios):
    while True:
        try:
            rut = int(input("Ingrese su rut(sin puntos y sin guion): "))
            if rut >= 5000000 and rut <= 30000000:
                if verificarRut(rut, usuarios):
                    print("El rut ya está registrado, ingrese uno distinto.")
                else:
                    return rut
            else:
                print("El rut debe ser un número entre 5000000 y 99999999")
        except:
            print("El rut debe ser un nùmero entero (5000000 y 99999999).")
            

def inputNombre():
    #Validación de nombre
    while True:
        try:
            nombre = input("Ingrese el nombre: ")
            parsedNombre = nombre.replace(" ", "")
            if len(nombre) <= 0 or len(parsedNombre) <= 0:
                print("No se permiten nombres vacios.")
            else:
                return nombre
        except:
            print("Ingrese un nombre válido.")    

def inputEdad():
    # Validación de edad
    while True:
        try:
            edad = int(input("Ingrese la edad: "))
            if edad >= 0 and edad <= 110:
                return edad
            else:
                print("La edad debe ser un número entre 0 y 110.")
        except:
            print("Número inválido")

def inputSexo():
    # Validacón de Sexo
    while True:
        sexo = input("Ingrese su sexo: (F o M) en mayúsculas o minúsculas: ")
        if sexo.upper() == "F" or sexo.upper() == "M":
            return sexo.upper()
        else:
            print("El sexo debe ser F o M.")
            
def inputPS():
    # Validación de PS
    while True:
        ps = input("Ingrese su PS: ")
        if ps.upper() == "ISAPRE" or ps.upper() == "FONASA":
            return ps.upper()
        else:
            ("Ps debe ser Isapre o Fonasa.")
            
def inputCorreo():
    # Validación de correo
    while True:
        correo = input("Ingrese su correo: ")
        if "@" in correo:
            return correo
        else:
            print("Debe ingresar un correo válido. (juanito@example.com)")
            
            
def atencionPaciente(usuarios):
# Menú para actualizar informacion de atencion de paciente.
    print("\t\tAtención paciente")
    try:
        rutAVerificar = int(input("Ingrese un rut a buscar: "))
        if verificarRut(rutAVerificar, usuarios):
            fechaAtencionInput = input("Ingrese la fecha de la atención: ")
            observacionesAtencionInput = input(
                "Ingrese las observaciones de la atención: "
            )
            for usuario in usuarios:
                if usuario["rut"] == rutAVerificar:
                    usuario["fechaAtencion"] = fechaAtencionInput
                    usuario["observacionesAtencion"] = (
                        observacionesAtencionInput
                    )
                    break
            print("Información actualizada. ")
            print("Volviendo al menú...")
            time.sleep(1.5)
        else:
            print("No existe usuario con el rut ingresado.")
            print("Volviendo al menú...")
            time.sleep(1.5)
    except:
        print("Debe ingresar un rut válido. (número entero sin punto ni guion)")
        print("Volviendo al menú")
        time.sleep(1)