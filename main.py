import os
import time

from functions import *
os.system("cls")

# Lista de usuarios
usuarios = [
    {
        "rut": 5000001,
        "nombre": "Juana Gómez",
        "edad": 25,
        "sexo": "F",
        "ps": "ISAPRE",
        "correo": "juanita@example.com",
        "fechaAtencion": "",
        "observacionesAtencion": "",
    },
    {
        "rut": 19905244,
        "nombre": "Kevin Peña",
        "edad": 25,
        "sexo": "M",
        "ps": "FONASA",
        "correo": "ke.penas@duocuc.cl",
        "fechaAtencion": "",
        "observacionesAtencion": "",
    },
]

# Variables de usuario(props)
rut = 0
edad = 0
sexo = ""
ps = ""
correo = ""
nombre = ""


def imprimirUsuario(usuario):
    print(
        f""" 
              Datos del Usuario:
    ------------------
    RUT: {usuario['rut']}
    Nombre: {usuario['nombre']}
    Edad: {usuario['edad']}
    Sexo: {usuario['sexo']}
    Plan de Salud: {usuario['ps']}
    Correo: {usuario['correo']}
    
              Datos de Atención:
    ------------------
    Fecha Atención: {usuario['fechaAtencion']}
    Observaciones: {usuario['observacionesAtencion']}
          """
    )
    

def registroPaciente():
    os.system("cls")
    print("\t\tRegistro de Paciente")
    
    rut = inputRut(usuarios)
    nombre = inputNombre()
    edad = inputEdad()
    sexo = inputSexo()
    ps = inputPS()   
    correo = inputCorreo()

    # Se crea el nuevo usuario:
    usuarioObj = {
        "rut": rut,
        "nombre": nombre,
        "edad": edad,
        "sexo": sexo,
        "ps": ps,
        "correo": correo,
        "fechaAtencion": "",
        "observacionesAtencion": "",
    }
    usuarios.append(usuarioObj)



while True:
    print("\t\tCentro Médico DUOC")
    print(
        "1) Registrar Paciente\n2) Atención Paciente\n3) Gestionar Paciente\n4) Salir"
    )
    try:
        menuOption = int(input("Ingrese una opción: "))

        if menuOption == 1:
            while True:
                registroPaciente()
                if int(input("Desea agregar otro paciente ?\n1. Si\n2. No\n: ")) != 1:
                    break

        elif menuOption == 2:
            atencionPaciente(usuarios)
            
        elif menuOption == 3:
            print("\t\tGestionar paciente")
            while True: 
                os.system("cls")
                print(
                    "1) Consultar datos paciente.\n2) Eliminar paciente\n3) Modificar paciente\n4) Regresar al menú principal"
                )
                try:
                    pacienteMenuOption = int(input("Ingrese una opción: "))

                    if pacienteMenuOption == 1:
                        print(usuarios)
                        rutABuscarInput = int(input("Ingrese un rut a buscar: "))
                        if verificarRut(rutABuscarInput, usuarios):
                            for usuario in usuarios:
                                if usuario["rut"] == rutABuscarInput:
                                    imprimirUsuario(usuario)
                                    input("Pulse enter para continuar")
                        else:
                            print("No existe usuario con ese rut.")
                            time.sleep(1.5)
                    elif pacienteMenuOption == 2:
                        rutABuscarInput = int(input("Ingrese un rut a buscar: "))
                        if verificarRut(rutABuscarInput, usuarios):
                            for usuario in usuarios:
                                if usuario["rut"] == rutABuscarInput:
                                    usuarios.remove(usuario)
                                    print("Usuario eliminado con éxito.")
                                    time.sleep(1.5)
                                    break
                        else:
                            print("No existe usuario con ese rut.")
                            time.sleep(1.5)
                    elif pacienteMenuOption == 3:
                        print("Opcion 3 - Modificar usuario")
                    elif pacienteMenuOption == 4:
                        break

                except:
                    print("Debe ingresar un número válido.")

        elif menuOption == 4:
            print("Ha salido del sistema!")
            time.sleep(2)
            break
        else:
            print("Ingrese una de las opciones listadas.")
            time.sleep(1)
            os.system("cls")
    except:
        print("Debe ingresar una opción válida.")
